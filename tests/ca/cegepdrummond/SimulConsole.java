package ca.cegepdrummond;

import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Permet de simuler la console pour faire les tests
 * <p>
 * Le main démarre avant le début du test.
 * Vous devez vous assurez que le main se termine à la fin de votre test.
 * <p>
 * Utilisez ecrire("texte") pour écrire du texte dans la console.
 * <p>
 * Utilisez getSortie() pour obtenir le texte afficher dans la console par votre application.
 * <p>
 * Utilisez assertSortie("texte") pour vérifier si la sortie correspond au texte exactement avec saut de ligne.
 * <p>
 * Utilisez assertSortieContient("texte") pour véfirier si la sortie contient le texte.
 */
public abstract class SimulConsole {
    private static final InputStream stdIn = System.in;
    private static final PrintStream stdOut = System.out;
    private ByteArrayOutputStream testSortie;

    private static final List<String> entrees = new ArrayList<>();
    private static final List<String> bufferSortie = new ArrayList<>();
    private static int pos;
    private static boolean sortieTerminee = false;
    private Thread threadMain;

    /**
     * Permet d'écrire du texte dans la console
     *
     * @param entree Texte à écrire dans la console
     */
    protected synchronized void ecrire(String entree) {
        afficherSortieEtFlush();
        entrees.add(entree + System.lineSeparator());
        try {
            while (!entrees.isEmpty() && threadMain.isAlive()) {
                Thread.sleep(10);
            }
        } catch (InterruptedException ignored) {
        }
    }


    /**
     * Permet d'obtenir la prochaine ligne affichée.
     * Si plusieurs lignes étaient affichées, elles sont mis dans un buffer, et retournées
     * une à la fois.
     *
     * @return Texte affiché dans la console
     */
    protected synchronized String getSortie() {
        String a;
        try {
            while (!sortieTerminee && threadMain.isAlive()) {
                Thread.sleep(10);
            }
        } catch (InterruptedException ignored) {
        }
        if (testSortie.size() > 0) {
            String[] arrOfString = testSortie.toString(StandardCharsets.UTF_8).split(System.lineSeparator(), 0);
            testSortie.reset();
            for (String uneString : arrOfString) {
                bufferSortie.add(uneString);
            }
        }

        if (bufferSortie.size() == 0) {
            a = "";
        } else {
            a = bufferSortie.remove(0);
        }
        try {
            stdOut.write((a+System.lineSeparator()).getBytes());
            stdOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return a;
    }

    /**
     * Permet de vérifier si le texte afficher en console correspond exactement à sortie avec un saut de ligne
     *
     * @param sortie Texte attendu dans la console
     */
    protected void assertSortie(String sortie) {
        assertSortie(sortie, true);
    }

    /**
     * Permet de vérifier si le texte afficher en console correspond exactement à sortie avec un saut de ligne ou non
     *
     * @param sortie          Texte attendu dans la console
     * @param avecSautDeLigne Ajoute un saut de ligne à sortie ou non
     */
    protected void assertSortie(String sortie, boolean avecSautDeLigne) {
        if (avecSautDeLigne) {
            sortie += System.lineSeparator();
        }
        assertEquals(sortie, getSortie());

    }

    /**
     * Permet de vérifier si le texte affiché dans la console contient contenu
     *
     * @param contenu Contenu attendu dans le texte affiché dans la console
     */
    protected void assertSortieContient(String contenu) {
        assertTrue(getSortie().contains(contenu));
    }

    /**
     * Permet de vérifier si le test affiché dans la console est une série de lignes
     * contenant chacun des éléments de la string en paramètre séparée en ligne en coupant
     * au espace.
     */
    protected void assertSortieMultiLignes(String sortie) {
        String[] sorties = sortie.split(" ");
        for(String ligne: sorties) {
            assertSortie(ligne,false);
        }
    }
    @BeforeEach
    private void configureESEtDemarreMain() {
        pos = 0;
        InputStream testEntree = new InputStream() {
            @Override
            public synchronized int read() throws IOException {
                if (entrees.isEmpty()) {
                    sortieTerminee = true;
                }
                try {
                    while (entrees.isEmpty()) {
                        Thread.sleep(10);
                    }
                } catch (InterruptedException ignored) {
                    throw new IOException("Main killed");
                }

                if (pos >= entrees.get(0).length()) {
                    entrees.remove(0);
                    pos = 0;
                    return -1;
                }


                char resultat = entrees.get(0).charAt(pos++);
                stdOut.write(resultat);
                stdOut.flush();
                return resultat;
            }
        };

        System.setIn(testEntree);
        //Entree.setScanner(new Scanner(System.in));

        testSortie = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testSortie));

        threadMain = new Thread(() -> {
            try {
                Main.main(new String[]{});
            } catch (InputMismatchException e ) {
                System.err.println("Mauvais type de données en entrée, ou donnée hors intervalle");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sortieTerminee = true;
            }
        });
        threadMain.start();
    }

    @AfterEach
    private synchronized void remetEntreeSortie() throws InterruptedException {
        sortir(); // ****** ATTENTION pour sortir du menu... a changer selon l'application qui utilise cette classe.
        afficherSortieEtFlush();
        entrees.clear();
        System.setIn(stdIn);
        System.setOut(stdOut);
        threadMain.interrupt();
    }

    private synchronized void afficherSortieEtFlush() {
        try {
            while (!sortieTerminee && threadMain.isAlive()) {
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            stdOut.write(testSortie.toByteArray());
            stdOut.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        testSortie.reset();
        sortieTerminee = false;
    }



    // Utilitaires

    /**
     * Fonction utilitaire pour passer au traver du menu.
     * Lecture d'une ligne d'output (la question du menu) et
     * envoie d'un choix de menu.
     * @param choix
     */
    protected void choixMenu(String choix ) {
        String a1 = getSortie(); // gobe le prompt
        ecrire(choix);
    }

    protected void sortir() {
        choixMenu("0");
    }
}
